<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { 
	foreach($error_warning as $err) {
  ?>
  <div class="warning"><?php echo $err; ?></div>
  <?php } } ?>
  <?php if ($success) { 
  ?>
  <div class="success"><?php echo $success; ?></div>
  <?php }  ?>
  
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#robokassa_stay').attr('value', '0'); $('#form').submit();" class="button"><span><?php echo $button_save_and_go; ?></span></a><a onclick="$('#robokassa_stay').attr('value', '1'); $('#form').submit();" class="button"><span><?php echo $button_save_and_stay; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
	
	
      <form action="<?php echo $action; ?>" method="post"  autocomplete="off" enctype="multipart/form-data" id="form">
	  <input type="hidden" name="robokassa_stay" id="robokassa_stay" value="0">
        <table class="form">
		  <tr>
			<td colspan=2>
				<b><?php echo $notice; ?></b>
			</td>
		  </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="robokassa_status">
                <?php if ($robokassa_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_shop_login; ?></td>
            <td><input type="text" name="robokassa_shop_login" value="<?php echo $robokassa_shop_login; ?>" /></td>
          </tr>
          <tr>
            <td><?php echo $entry_password1; ?></td>
            <td><input type="password" name="robokassa_password1" /></td>
          </tr>
		  <tr>
            <td><?php echo $entry_password2; ?></td>
            <td><input type="password" name="robokassa_password2" /></td>
          </tr>
		  
		  <tr>
            <td><?php echo $entry_result_url; ?>:</td>
            <td>http://<?php echo $_SERVER['HTTP_HOST']; ?>/index.php?route=payment/robokassa/result</td>
          </tr>
          
		  <tr>
            <td><?php echo $entry_result_method; ?></td>
            <td>POST</td>
          </tr>
          
		  <tr>
            <td><?php echo $entry_success_url; ?></td>
            <td>http://<?php echo $_SERVER['HTTP_HOST']; ?>/index.php?route=checkout/success</td>
          </tr>
          
		  <tr>
            <td><?php echo $entry_success_method; ?></td>
            <td>POST</td>
          </tr>
          
		  <tr>
            <td><?php echo $entry_fail_url; ?></td>
            <td>http://<?php echo $_SERVER['HTTP_HOST']; ?>/index.php?route=checkout/checkout</td>
          </tr>
          
		  <tr>
            <td><?php echo $entry_fail_method; ?></td>
            <td>POST</td>
          </tr>
          
		  
		  
		  <tr>
            <td><?php echo $entry_test_mode; ?></td>
            <td><select name="robokassa_test_mode">
                <?php if ($robokassa_test_mode) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          
		  
		  <tr>
            <td><?php echo $entry_order_status; ?></td>
            <td><select name="robokassa_order_status_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $robokassa_order_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
		  <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="robokassa_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $robokassa_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="robokassa_sort_order" value="<?php echo $robokassa_sort_order; ?>" size="1" /></td>
          </tr>
		  
		  <tr>
            <td colspan=2><b><?php echo $entry_methods; ?></b></td>           
          </tr>
		  <?php if( !$robokassa_shop_login ) { ?>
		  <tr>
            <td colspan=2><b><font color=red><?php echo $entry_no_methods; ?></font></b></td>           
          </tr>
		  <?php } elseif( !$currencies ) { ?>
		  <tr>
            <td colspan=2><b><font color=red><?php echo $entry_no_robokass_methods; ?></font></b></td>           
          </tr>
		  <?php } else { ?>
		   <tr>
            <td colspan=2>
				<style>
					.methods td, .methods th
					{
						border: 1px dotted #ccc;
					}
					
					.methods					
					{
						border: 1px dotted #ccc;
					}
				</style>
				<table cellpadding=5 cellspacing=0 class="methods">
					<tr>
						<th valign=top>#</th>
						<th valign=top><?php echo $methods_col1; ?></th>
						<th valign=top><?php echo $methods_col2; ?></th>
					</tr>
					<?php for($i=0; $i<10; $i++) { ?>
					<tr>
						<td>#<?php echo ($i+1); ?></td>
						<td><input type="text" size=60 name="robokassa_methods[<?php echo $i; ?>]" value="<?php echo $robokassa_methods[$i]; ?>"></td>
						<td><select name="robokassa_currencies[<?php echo $i; ?>]">
							<option value="0"><?php echo $select_currency; ?></option>
						<?php foreach($currencies as $key=>$val) { ?>
							<option value="<?php echo $key; ?>" <?php 
							if( $robokassa_currencies[$i]==$key ) { ?>selected<?php }?>
							><?php echo $val; ?></option>
						<?php } ?>
						</select></td>
					</tr>
					<?php } ?>
				</table>
			</td>           
          </tr>
		  <?php } ?>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 