<?php /* robokassa metka */
// Heading
$_['heading_title']      = 'RoboKassa';

// Text 
$_['text_success']       = 'Success: You have modified RoboKassa account details!';
      
// Entry
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_shop_login']   = 'Shop Login (your login in robokassa.ru):';
$_['entry_order_status2'] = 'Order status which the buyer refused to pay:';

$_['entry_test_mode'] 	 = 'Test Mode:';
$_['entry_password1'] 	 = 'Password #1:';
$_['entry_password2'] 	 = 'Password #2:';

$_['notice'] 	 		 = 'Module settings should match the settings in your account on the site robokassa.ru <a href="https://www.roboxchange.com/Environment/Partners/Login/Merchant/Administration.aspx" target=_blank>at the link</a>.';

$_['entry_result_url']	  = 'Result URL:';
$_['entry_result_method'] = "Result URL Method:";

$_['entry_success_url']    = "Success URL:";
$_['entry_success_method'] = "Success URL Method:";

$_['entry_fail_url']    = "Fail URL:";
$_['entry_fail_method'] = "Fail URL Method:";

$_['select_currency']	  = 'Choose currency';
$_['entry_no_robokass_methods']	  = 'To Shop Login you chose no method of payment. Perhaps you entered an incorrect Login';

$_['methods_col1'] = 'Displayed payment method';
$_['methods_col2'] = 'Currency by default after redirect to robokassa <br>(The customer will be able to change currency)';
$_['text_payment'] = 'Modules'; 
// Error



$_['error_permission']   = 'Warning: You do not have permission to modify payment RoboKassa!';
$_['error_robokassa_shop_login'] = 'Please specify Shop Login';
$_['error_robokassa_password1']	  = 'Please enter Password1';
$_['error_robokassa_password2']	  = 'Please enter Password2';
$_['error_rub']	  = 'Ruble currency must be added to the list in System => Localisation => Currencies. Currency Code must be a RUB or RUR';
$_['button_save_and_go']	  = 'Save and go';
$_['button_save_and_stay']	  = 'Save and stay';

$_['entry_no_methods']	  = 'Methods will appear after you enter Shop Login';
$_['entry_methods']	  = 'Displayed payment methods';

?>