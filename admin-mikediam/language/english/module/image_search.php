<?php
// Heading
$_['heading_title']    = 'Image search';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Image search!';

// Text
$_['text_success']     = 'Success: You have modified Image search!';

$_['button_save']      = 'Save';
$_['button_cancel']    = 'Cancel';

$_['search_page_go_text']    = 'Go to search image page';

$_['image_path'] = 'Path to save images /image/';
$_['product_count'] = 'Products count';
$_['search_where'] = 'Where to search';
$_['search_where_model'] = 'In model';
$_['search_where_name'] = 'In name';
$_['search_where_sku'] = 'In sku';
$_['server_timeout'] = 'Wait for server response, milliseconds';
$_['site_search'] = 'Search on site';
$_['safe_search'] = 'Safe search';
$_['safe_search_strict'] = 'Filter name and image';
$_['safe_search_moderate'] = 'Filter image';
$_['safe_search_off'] = 'Off';
$_['image_size'] = 'Image size';
$_['image_size_small'] = 'Small (<100px)';
$_['image_size_medium'] = 'Medium (250px-1000px)';
$_['image_size_large'] = 'Large (1000px-1500px)';
$_['image_size_extra_large'] = 'Extra large (>1500px)';
$_['image_colorization'] = 'Colorization';
$_['image_colorization_grayscale'] = 'Grayscale';
$_['image_colorization_color'] = 'Color';
$_['image_color_filter'] = 'Color filter';
$_['image_color_filter_black'] = 'Black';
$_['image_color_filter_blue'] = 'Blue';
$_['image_color_filter_brown'] = 'Brown';
$_['image_color_filter_gray'] = 'Gray';
$_['image_color_filter_green'] = 'Green';
$_['image_color_filter_orange'] = 'Orange';
$_['image_color_filter_pink'] = 'Pink';
$_['image_color_filter_purple'] = 'Purple';
$_['image_color_filter_red'] = 'Red';
$_['image_color_filter_teal'] = 'Teal';
$_['image_color_filter_white'] = 'White';
$_['image_color_filter_yellow'] = 'Yellow';
$_['file_type'] = 'File type';
$_['file_type_jpg'] = 'JPG';
$_['file_type_png'] = 'PNG';
$_['file_type_gif'] = 'GIF';
$_['file_type_bmp'] = 'BMP';
$_['image_type'] = 'Image type';
$_['image_type_faces'] = 'Faces';
$_['image_type_photo'] = 'Photo';
$_['image_type_clipart'] = 'Clipart';
$_['image_type_lineart'] = 'Lineart';
$_['image_rights'] = 'Rights';
$_['image_rights_reuse'] = 'Reuse';
$_['image_rights_commercial_reuse'] = 'Commercial reuse';
$_['image_rights_modification'] = 'Modification';
$_['image_rights_commercial_modification'] = 'Commercial modification'; 
$_['image_rights_more_link'] = '<a href="http://support.google.com/websearch/bin/answer.py?hl=en&hlrm=ru&answer=29508" target="_blank">More</a>'; 
