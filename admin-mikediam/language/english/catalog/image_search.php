<?php
// Heading
$_['heading_title']          = 'Image search';

// Column
$_['column_image']           = 'Image';
$_['column_status']          = 'Status';
$_['column_stock_status']    = 'Stock status';
$_['column_category']        = 'Category';

$_['image_status_no_image']  = 'No image';
$_['image_status_all_images']= 'All images';
$_['image_status_only_main'] = 'Only main';

$_['image_path'] = 'Path to save images /image/';

// Button
$_['button_search']          = 'Search';

// Entry
$_['entry_name']             = 'Name:';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify images!';

$_['error_unlink_file_not_exists']   = 'Error deleting file, file not found';
$_['error_unlink_file_not_writable'] = 'Failed to delete file, file is not writable';
$_['error_unlink_file_not_image']    = 'The file is not a image';
$_['error_unlink_directory_not_writable']    = 'Destination directory is not writable';
$_['error_unlink_save_file']         = 'Error saving file';
$_['error_unlink_file_not_loaded']   = 'Could not load file';
$_['error_ext_not_support']          = 'File extension not support';
$_['error_cant_create_directory']    = 'Could not create directory';
