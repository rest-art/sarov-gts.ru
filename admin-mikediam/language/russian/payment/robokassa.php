<?php /* robokassa metka */
// Heading
$_['heading_title']      = 'RoboKassa';

// Text 
$_['text_success']       = 'Выполнено: Вы изменили настройки модуля RoboKassa!';
      
// Entry
$_['entry_geo_zone']     = 'Географическая зона:';
$_['entry_status']       = 'Статус:';
$_['entry_sort_order']   = 'Порядок сортировки:';
$_['entry_order_status'] = 'Статус нового оплаченного заказа:';
$_['entry_order_status2'] = 'Статус заказа от которого покупатель отказался:';
$_['entry_shop_login']   = '*Ваш логин на сайте http://robokassa.ru:';

$_['entry_test_mode'] 	 = 'Тестовый режим:';

$_['notice'] 	 		 = 'Настройки модуля должны соответствовать настройкам в Вашем аккаунте на сайте robokassa.ru <a href="https://www.roboxchange.com/Environment/Partners/Login/Merchant/Administration.aspx" target=_blank>по ссылке</a>.';

$_['entry_password1'] 	 = 'Пароль #1:';
$_['entry_password2'] 	 = 'Пароль #2:';

$_['entry_result_url']	 = 'Result URL';
$_['entry_result_method'] = "Метод отсылки данных по Result URL:";

$_['entry_success_url'] = "Success URL:";
$_['entry_success_method'] = "Метод отсылки данных по Success URL:";

$_['entry_fail_url'] = "Fail URL:";
$_['entry_fail_method'] = "Метод отсылки данных по Fail URL:";

$_['entry_no_methods']	  = 'Методы появятся после того как Вы укажите Логин';
$_['entry_methods']	  = 'Отображаемые методы оплаты';

$_['entry_no_robokass_methods']	  = 'Произошла ошибка соединения с РобоКассой, либо для выбранного логина нет методов оплаты. Попробуйте перезагрузить страницу.';

$_['methods_col1'] = 'Отображаемый платёжный метод';
$_['methods_col2'] = 'Валюта по-умолчанию после перехода на сайт robokassa.ru<br>(покупатель будет иметь возможность сменить валюту)';

$_['text_payment'] = 'Модули';
$_['select_currency']	  = 'Выбрать валюту';

// Error
$_['error_permission']   = 'У Вас нет прав для управления модулем RoboKassa!';
$_['error_robokassa_shop_login'] = 'Пожалуйста укажите Ваш логин на сайте http://robokassa.ru';
$_['error_robokassa_password1']	  = 'Не указан Пароль #1';
$_['error_robokassa_password2']	  = 'Не указан Пароль #2';

$_['error_rub']	  = 'Для работы метода необходимо добавить рубль в список валют в разделе Система=> Локализация => Валюты. Код валюты должен быть RUB или RUR';

$_['button_save_and_go']	  = 'Сохранить и выйти';
$_['button_save_and_stay']	  = 'Сохранить и остаться';

?>