<?php /* robokassa metka */
class ControllerPaymentRobokassa extends Controller {
	private $error = array(); 

	public function index() {
		$this->load->language('payment/robokassa');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if( !$this->request->post['robokassa_password1'] 
			&& $this->config->get('robokassa_password1') )
			$this->request->post['robokassa_password1'] = $this->config->get('robokassa_password1');
			
			if( !$this->request->post['robokassa_password2'] 
			&& $this->config->get('robokassa_password2') )
			$this->request->post['robokassa_password2'] = $this->config->get('robokassa_password2');
			
			$ext_arr = array();
			$updExt = array();
			
			if( !empty($this->request->post['robokassa_methods']) )
			{
				$i=0;
				foreach( $this->request->post['robokassa_methods'] as $val )
				{
					if( $val && !empty($this->request->post['robokassa_currencies'][$i]) )
					{
						if($i!=0)
						{
							$this->request->post['robokassa'.$i.'_status'] = 1;
							$updExt[] = $i;
						}
					}
					else
					{
						if($i!=0)
						{
							$this->request->post['robokassa'.$i.'_status'] = 0;
						}
					}
				
					$i++;
				}
			}
			
			$this->model_setting_setting->editSetting('robokassa', $this->request->post);				
			
			$this->load->model('localisation/robokassa');
			$this->model_localisation_robokassa->updateExtentions($updExt);
			
			if($this->request->post['robokassa_stay'])
			$this->redirect($this->url->link('payment/robokassa', 'success=1&token=' . $this->session->data['token'], 'SSL'));
			else
			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		if( !empty($this->request->get['success'] ) )
		$this->data['success'] = $this->language->get('text_success');
		else
		$this->data['success'] = '';
		
		
		$this->load->model('localisation/currency');
		$results = $this->model_localisation_currency->getCurrencies();
		
		if( !isset($results['RUB']) && !isset($results['RUR']) )
		{
			$this->error[] = $this->language->get('error_rub');
		}			

		$this->data['notice'] = $this->language->get('notice');

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
				
		$this->data['entry_shop_login'] = $this->language->get('entry_shop_login');		
		
		$this->data['entry_test_mode'] = $this->language->get('entry_test_mode');
		
		$this->data['entry_result_url'] = $this->language->get('entry_result_url');
		$this->data['entry_result_method'] = $this->language->get('entry_result_method');
		$this->data['entry_success_url'] = $this->language->get('entry_success_url');
		$this->data['entry_success_method'] = $this->language->get('entry_success_method');
		$this->data['entry_fail_url'] = $this->language->get('entry_fail_url');
		$this->data['entry_fail_method'] = $this->language->get('entry_fail_method');
		
		$this->data['entry_no_methods'] = $this->language->get('entry_no_methods');
		$this->data['entry_methods'] = $this->language->get('entry_methods');
		
		
		$this->data['entry_password1'] = $this->language->get('entry_password1');		
		$this->data['entry_password2'] = $this->language->get('entry_password2');		
		
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');	
		$this->data['entry_order_status2'] = $this->language->get('entry_order_status2');		
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['entry_no_robokass_methods'] = $this->language->get('entry_no_robokass_methods');
		
		$this->data['select_currency'] = $this->language->get('select_currency');
	
		$this->data['methods_col1'] = $this->language->get('methods_col1');
	
		$this->data['methods_col2'] = $this->language->get('methods_col2');
	
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_save_and_go'] = $this->language->get('button_save_and_go');
		$this->data['button_save_and_stay'] = $this->language->get('button_save_and_stay');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		$currencies = array();
		$this->data['currencies'] = array();
		
		if (isset($this->request->post['robokassa_test_mode'])) {
			$this->data['robokassa_test_mode'] = $this->request->post['robokassa_test_mode'];
		} else {
			$this->data['robokassa_test_mode'] = $this->config->get('robokassa_test_mode'); 
		}
		
		if( $this->config->get('robokassa_shop_login') )
		{
		    if( $this->data['robokassa_test_mode'] )
			$url = "http://test.robokassa.ru/Webservice/Service.asmx/GetCurrencies?MerchantLogin=".$this->config->get('robokassa_shop_login')."&Language=ru";
			else
			$url = "http://merchant.roboxchange.com/Webservice/Service.asmx/GetCurrencies?MerchantLogin=".$this->config->get('robokassa_shop_login')."&Language=ru";
			
			if( extension_loaded('curl') )
			{
				$c = curl_init($url);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
				$page = curl_exec($c);
				curl_close($c);
			}
			else
			{
				$page = file_get_contents($url);
			}
			
			
			if( !preg_match("/<Code>0<\/Code>/i", $page) )
			{
				$this->data['robokassa_methods'] = '';
			}
			elseif($page)
			{
				$arr_value = array();
				$group_value = array();
				
				//<Group Code="EMoney" Description="Электронные валюты"></Group>
				//preg_match_all("/(<offer([^>]*)>(.*)<\/offer>)/Ui", $yml_data, $pred_offer);
				//preg_match_all("/(<Group Code=\"([^\"]+)\" Description=\"([^\"]+)\">(.*)<\/Group>)/Ui", $page, $group_value);
				
				$xml = simplexml_load_file($url);
				if( $xml )
				{
					//foreach($xml->Groups->Group[] as $val) echo $val."<hr>";
					//exit($xml->Groups->Group['Description'][0] );
					
					//echo $xml->Groups->Group['Description'][0]."<br>";
					
					
					
					foreach($xml->Groups->Group as $gr)
					{
					
						foreach($gr->Items->Currency as $v)
						{
							$currencies[ trim($v['Label'][0]) ] = $v['Name']." (".$gr['Description'][0].")"; 
						}
					}
					
					$this->data['currencies'] = $currencies;
				}
				else				
				{
					$this->data['robokassa_methods'] = '';
				}
			}
		}
		else
		{
			$this->data['robokassa_methods'] = '';
		}
		
  		if (isset($this->error)) {
			$this->data['error_warning'] = $this->error;
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/robokassa', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('payment/robokassa', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['callback'] = HTTP_CATALOG . 'index.php?route=payment/robokassa/callback';

		if (isset($this->request->post['robokassa_order_status_id'])) {
			$this->data['robokassa_order_status_id'] = $this->request->post['robokassa_order_status_id'];
		} else {
			$this->data['robokassa_order_status_id'] = $this->config->get('robokassa_order_status_id'); 
		} 
		
		if (isset($this->request->post['robokassa_order_status_id2'])) {
			$this->data['robokassa_order_status_id2'] = $this->request->post['robokassa_order_status_id2'];
		} else {
			$this->data['robokassa_order_status_id2'] = $this->config->get('robokassa_order_status_id2'); 
		} 
		
		if (isset($this->request->post['robokassa_shop_login'])) {
			$this->data['robokassa_shop_login'] = $this->request->post['robokassa_shop_login'];
		} else {
			$this->data['robokassa_shop_login'] = $this->config->get('robokassa_shop_login'); 
		} 
		
		if (isset($this->request->post['robokassa_methods'])) {
			$this->data['robokassa_methods'] = $this->request->post['robokassa_methods'];
		} else {
			$this->data['robokassa_methods'] = $this->config->get('robokassa_methods'); 
		} 		
		
		if (isset($this->request->post['robokassa_currencies'])) {
			$this->data['robokassa_currencies'] = $this->request->post['robokassa_currencies'];
		} else {
			$this->data['robokassa_currencies'] = $this->config->get('robokassa_currencies'); 
		} 		
		
		
		
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['robokassa_geo_zone_id'])) {
			$this->data['robokassa_geo_zone_id'] = $this->request->post['robokassa_geo_zone_id'];
		} else {
			$this->data['robokassa_geo_zone_id'] = $this->config->get('robokassa_geo_zone_id'); 
		} 

		$this->load->model('localisation/geo_zone');
										
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['robokassa_status'])) {
			$this->data['robokassa_status'] = $this->request->post['robokassa_status'];
		} else {
			$this->data['robokassa_status'] = $this->config->get('robokassa_status');
		}
		
		if (isset($this->request->post['robokassa_sort_order'])) {
			$this->data['robokassa_sort_order'] = $this->request->post['robokassa_sort_order'];
		} else {
			$this->data['robokassa_sort_order'] = $this->config->get('robokassa_sort_order');
		}

		$this->template = 'payment/robokassa.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/robokassa')) {
			$this->error[] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['robokassa_password1'] && !$this->config->get('robokassa_password1') ) {
			$this->error[] = $this->language->get('error_robokassa_password1');
		}
		
		if (!$this->request->post['robokassa_password2'] && !$this->config->get('robokassa_password2') ) {
			$this->error[] = $this->language->get('error_robokassa_password2');
		}

		if (!$this->request->post['robokassa_shop_login']) {
			$this->error[] = $this->language->get('error_robokassa_shop_login');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>