<?php if (!empty($info)) { ?>
    <h2><?php echo $text_instruction; ?></h2>
    <div class="content">
          <p><?php echo $text_description; ?></p>
          <p><?php echo $info; ?></p>
          <p><?php echo $text_payment; ?></p>
    </div>
<?php }  ?>

<div class="buttons">
  <div class="right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({ 
		type: 'get',
		url: 'index.php?route=payment/<?php echo $name; ?>/confirm',
		success: function() {
			location = '<?php echo $continue; ?>';
		}		
	});
});
//--></script> 
