<style type="text/css">
#menu {
	-webkit-border-radius: 5px 5px 0 0;
	-moz-border-radius: 5px 5px 0 0;;
	-khtml-border-radius: 5px 5px 0 0;;
	border-radius: 5px 5px 0 0;
}
</style>
<div id="wrapper">    
        <div class="slider-wrapper theme-orman">
<div class="slideshow">
            <div class="ribbon"></div>
            <div id="slider" class="nivoSlider">
 <div id="slideshow<?php echo $module; ?>" class="nivoSlider" style="100%;border: 1px solid #444;box-shadow: 0 0 5px 0 #080808; height: <?php echo $height; ?>px;">
    <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
    <?php } ?>

  </div>
</div>
<div class="controlNav_box"></div>
</div>
</div>
</div>
<br><br>
<br>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#slideshow<?php echo $module; ?>').nivoSlider();
});
--></script>
