<div id="notification"></div>
<div class="box">
  <div class="box-heading_left"><h3><?php echo $heading_title; ?></h3></div>
  <div class="box-content">
    <div class="box-product">
      <?php foreach ($products as $product) { ?>
      <div>
        <?php if ($product['thumb']) { ?>
	
	<div href="#" class="showhim">
        <div class="image">
	<div class="out_bg">

	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
<div class="showme">
	<div class="description_featured" style="min-height:110px;">	
	<?php echo "<p>".mb_substr(strip_tags(html_entity_decode($product['description'])), 0, 140)."...</p>"; ?></div>
</div></a>
	
	<?php if ($product['price']) { ?>	 
	<div class="priced">
	<ul>
	<?php if (!$product['special']) { ?>
        <li><?php echo $product['price']; ?></li>
          <?php } else { ?>   
	<div id="sale"></div>       

	 <li><span class="price-new"><?php echo $product['special']; ?></span> 
	 <span class="price-old" ><?php echo $product['price']; ?></span> </li>
	 
          <?php } ?>

	<li class="space"><a id="east1" title="Add to Product Comparison" class="compare_feat" onclick="addToCompare('<?php echo $product['product_id']; ?>');" ></a></li>
	<li class="space"><a id="south1"title="Add to Wishlist"  class="wishlist_feat" onclick="addToWishList('<?php echo $product['product_id']; ?>');" ></a></li>
	<li class="space"><a id="south1"title="Add to Cart" class="cart_feat" onclick="addToCart('<?php echo $product['product_id']; ?>');" ></a></li>
</ul>
</div>
</div>
<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

        <div style="margin-top:24px;"></div>

	</div>	
<div class="showme">
	<?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <?php } ?>
	
</div></div>

	
        <?php } ?>
    

 
        
      </div>
      <?php } ?>
    </div>
  </div>
</div>
