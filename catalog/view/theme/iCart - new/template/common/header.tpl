<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/iCart/tooltip/jquery.tipsy.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php echo $google_analytics; ?>
</head>
<body
<?php 		
	$page = '';
	if(isset($this->request->get['route'])){
		$page = $this->request->get['route']; 
	}
	if($page == "common/home" || $page == ''){
		echo 'class="home"';

	}elseif($page == "product/category"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
		echo 'class="' . strtolower($page) . " category" . '"';		
	}

	elseif($page == "account/wishlist" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "account/account" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "checkout/cart" || $page == ''){
		echo 'class="home"';	
	}
	elseif($page == "checkout/checkout" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "product/product"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
		echo 'class="' . strtolower($page) . " product_page" . '"';		
	}
	elseif($page !== "common/home"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
			if(isset($titleName[1])){
				$page = $titleName[0] . "_" . $titleName[1];
			}
		echo 'class="' . strtolower($page) . '"';				
	}


	

?>>
    
    <script type="text/javascript">
  $(function() { 
 $("#north1:nth-child(1n-1)").tipsy({gravity: "n"});
 $("#south1:nth-child(1n-1)").tipsy({gravity: "s"});
 $("#west1:nth-child(1n-1)").tipsy({gravity: "w"});
 $("#east1:nth-child(1n-1)").tipsy({gravity: "e"});
});
</script>

<style type="text/css">

#container_bg {
	width:1000px;
	margin-left: auto;
	margin-right: auto;
	text-align: left;
   	background: none;
	border: none;
	margin-bottom:none;
	padding-left:none;
	padding-right:none;
	padding-top:0;
	border-radius:none;
	padding-top:0;
	margin-top:0;
{

</style>

<div class="linkz">
<ul>
<li <?php if($page == "common/home" || $page == ''){echo 'class="active"';} ?>><a class="home" href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
<li <?php if($page == "account/wishlist" || $page == 'account/wishlist'){echo 'class="active"';} ?>><a class="wishlist" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
<li <?php if($page == "account/account" || $page == 'account/account'){echo 'class="active"';} ?>><a class="account" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
<li <?php if($page == "checkout/cart" || $page == 'checkout/cart'){echo 'class="active"';} ?>><a class="cart" href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
<li <?php if($page == "checkout/checkout" || $page == 'checkout/checkout'){echo 'class="active"';} ?>><a class="checkout"href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>

</ul>
</div>

<div id="container">
<div id="header">
<?php if ($logo) { ?> <div id="logo"><a href="<?php echo $home; ?>"><img alt="<?php echo $name; ?>"  title="<?php echo $name; ?>" src="catalog/view/theme/iCart/image/logo.png"/></a></div>  <?php } ?>
<?php echo $language; ?>
<?php echo $currency; ?>
<?php echo $cart; ?>
<div>  
<div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
  </div>

  <div id="search">
    <div class="button-search"></div>
    <?php if ($filter_name) { ?>
    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
    <?php } else { ?>
    <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#888';" />
    <?php } ?>
  </div>
</div>
</div>


<?php if ($categories) { ?>
<div id="menu">
  <ul>

    <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
	

      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>

        <ul>	

          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
         </ul>
        <?php } ?>
      </div>
<img src="catalog/view/theme/iCart/image/arrow-down_menu.png" style="margin-bottom:-4px;margin-right:11px;margin-left:-14px;"/>
	<?php } ?>
    </li>
    <?php } ?>

  </ul>
</div>
</div>
</div>

<div id="container_bg">
<?php } ?>

