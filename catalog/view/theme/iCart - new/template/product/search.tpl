<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <b><?php echo $text_critea; ?></b>
  <div class="content">
    <p><?php echo $entry_search; ?>
      <?php if ($filter_name) { ?>
      <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
      <?php } else { ?>
      <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
      <?php } ?>
      <select name="filter_category_id">
        <option value="0"><?php echo $text_category; ?></option>
        <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['category_id'] == $filter_category_id) { ?>
        <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <?php if ($category_2['category_id'] == $filter_category_id) { ?>
        <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_2['children'] as $category_3) { ?>
        <?php if ($category_3['category_id'] == $filter_category_id) { ?>
        <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
      </select>
      <?php if ($filter_sub_category) { ?>
      <input type="checkbox" name="filter_sub_category" value="1" id="sub_category" checked="checked" />
      <?php } else { ?>
      <input type="checkbox" name="filter_sub_category" value="1" id="sub_category" />
      <?php } ?>
      <label for="sub_category"><?php echo $text_sub_category; ?></label>
    </p>
    <?php if ($filter_description) { ?>
    <input type="checkbox" name="filter_description" value="1" id="description" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="filter_description" value="1" id="description" />
    <?php } ?>
    <label for="description"><?php echo $entry_description; ?></label>
  </div>
  <div class="buttons">
    <div class="right"><a id="button-search" class="button"><span><?php echo $button_search; ?></span></a></div>
  </div>
  <h2><?php echo $text_search; ?></h2>
  <?php if ($products) { ?>
        <div class="product-filter column">
        	<div class="limit">
                <dl class="btn-drop">
                    <dt></dt>
                    <?php 
                        $__selected = '';
                        foreach ($limits as $__limits) {
                            if ($__limits['value'] == $limit || $__selected == '') {
                                $__selected = $__limits['text'];
                            }
                        }
                    ?>
                    <dd><?php echo $__selected; ?></dd>
					<select onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            	<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            	<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </dl>
                <?php echo $text_limit; ?>
            </div>
            <div class="sort">
                <dl class="btn-drop">
                    <dt></dt>
                    <?php 
                        $__selected = '';
                        foreach ($sorts as $__sorts) {
                            if ($__sorts['value'] == $sort . '-' . $order || $__selected == '') {
                                $__selected = $__sorts['text'];
                            }
                        }
                    ?>
                    <dd><?php echo $__selected; ?></dd>
                    <select onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </dl>
                <?php echo $text_sort; ?>
            </div>
            <div class="display">
                <div class="cat_nav">
                    <a title="<?php echo $text_list; ?>" class="view-list active" onclick="display('list');">&nbsp;&nbsp;List<span class="icon icon-list">&nbsp;</span></a><a title="<?php echo $text_grid; ?>" class="view-grid" onclick="display('grid');">&nbsp;&nbsp;Grid<span class="icon icon-grid">&nbsp;</span></a>
                </div>
                <?php echo $text_display; ?>
            </div>
            <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
            <div class="clearfix"></div>
        </div>

	<br>
        <div class="product-list">
        	<?php foreach ($products as $product) { ?>
		<div class="struct">
                	<div class="struct-right"></div>
                    <div class="struct-left"></div>
                    <div class="struct-center">
                    	<div class="line">
			
                            <a href="<?php echo $product['href']; ?>" class="image-link">
                                <div class="image">
                                    <?php if ($product['thumb']) { ?>
                                        <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } else { ?>
                                        <div class="no-image"></div>
                                    <?php } ?>
                                </div>
                            </a>
                            
                            <div class="right">
                                <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                
                                <div class="cart cat_nav">
                                    <a onclick="addToCompare('<?php echo $product['product_id']; ?>');" title="<?php echo $button_compare; ?>">&nbsp;&nbspCompare<span class="icon icon-compare">&nbsp;</span></a><a onclick="addToCart('<?php echo $product['product_id']; ?>');" title="<?php echo $button_cart; ?>"><span class="icon icon-basket">&nbsp;</span></a><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" title="<?php echo $button_wishlist; ?>">&nbsp;&nbsp;Wish List<span class="icon icon-wish">&nbsp;</span></a>
                                </div>
                            </div>
                            
                            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['rating']) { ?>
                                <div class="rating" title="<?php echo $product['reviews']; ?>"><div class="stars-<?php echo $product['rating']; ?>"></div><span></span></div>
                            <?php } ?>
                            <div class="description"><?php echo $product['description']; ?></div>
                            <div class="clearfix"></div>
                        </div>
        			</div>
            </div><?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="column">
        	<div class="pagination"><?php echo $pagination; ?></div>
        </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php }?>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#content input[name=\'filter_name\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var filter_name = $('#content input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_category_id = $('#content select[name=\'filter_category_id\']').attr('value');
	
	if (filter_category_id > 0) {
		url += '&filter_category_id=' + encodeURIComponent(filter_category_id);
	}
	
	var filter_sub_category = $('#content input[name=\'filter_sub_category\']:checked').attr('value');
	
	if (filter_sub_category) {
		url += '&filter_sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'filter_description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&filter_description=true';
	}

	location = url;
});

function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			var image = $(element).find('.image-link').html();
			var price = $(element).find('.price').html();
			var rating = $(element).find('.rating').html();
			var cart = '<div class="cart cat_nav">' + $(element).find('.cart').html() + '</div>';
			
			image = (image != null) ? '<a href="' + $(element).find('.image-link').attr('href') + '" class="image-link">' + image + '</a>' : '';
			price = (price != null) ? '<div class="price">' + price  + '</div>' : '';
			rating = (rating != null) ? '<div class="rating" title="' + $(element).find('.rating').attr('title') + '">' + rating + '</div>' : '';
			
			

			html = 
				'<div class="struct-right"></div>' +
				'<div class="struct-left"></div>' +
				'<div class="struct-center">' +
					'<div class="line">' +
						image +
						'<div class="right">' + price + cart + '</div>' +
						'<div class="name">' + $(element).find('.name').html() + '</div>' +
						rating + 
						'<div class="description">' + $(element).find('.description').html() + '</div>' +
						'<div class="clearfix"></div>' + 
					'</div>' +
				'</div>';

						
			$(element).html(html);
		});		
				
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.cookie('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}	
					
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
						
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.cookie('display', 'grid');
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>