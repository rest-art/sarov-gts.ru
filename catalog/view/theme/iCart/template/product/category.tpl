<?php echo $header; ?>

<style type="text/css">

.linktree {
	margin-top:0px;
	margin-bottom:10px;
	margin-left:3px;
}


.linktree a {
	color:#666;
	text-decoration:none;
	font-size:11px;
}

.box-product {
	width: 100%;
	overflow: auto;
	margin-left:15px;
}

</style>

<div class="linktree">
<ul>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>

    <?php } ?>

</ul>
</div>

<br>
  
<h1 style="margin-top:-10px;"><?php echo $heading_title; ?></h1>

<br>


<div id="notification"></div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<?php echo $content_top; ?>
    
    <?php if ($thumb || $description) { ?>
        <div class="category-info">
            <?php if ($thumb) { ?>
            	<div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
            <?php } ?>
            <?php if ($description) { ?>
            	<?php echo $description; ?>
            <?php } ?>
        </div>
    <?php } ?>
    
    <?php if ($categories) { ?>
        <div class="column">
            <div class="category-list block">
                <h2><?php echo $text_refine; ?></h2>
                <?php if (count($categories) <= 5) { ?>
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        	<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } else { ?>
                    <?php for ($i = 0; $i < count($categories);) { ?>
                    	<div class="column20p">
                            <ul>
                                <?php $j = $i + ceil(count($categories) / 4); ?>
                                <?php for (; $i < $j; $i++) { ?>
                                    <?php if (isset($categories[$i])) { ?>
                                        <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>
                                    <?php } ?>	
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
  
	<?php if ($products) { ?>
        <div class="product-filter column">
        	<div class="limit">
                <dl class="btn-drop">
                    <dt></dt>
                    <?php 
                        $__selected = '';
                        foreach ($limits as $__limits) {
                            if ($__limits['value'] == $limit || $__selected == '') {
                                $__selected = $__limits['text'];
                            }
                        }
                    ?>
                    <dd><?php echo $__selected; ?></dd>
					<select onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            	<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            	<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </dl>
                <?php echo $text_limit; ?>
            </div>
            <div class="sort">
                <dl class="btn-drop">
                    <dt></dt>
                    <?php 
                        $__selected = '';
                        foreach ($sorts as $__sorts) {
                            if ($__sorts['value'] == $sort . '-' . $order || $__selected == '') {
                                $__selected = $__sorts['text'];
                            }
                        }
                    ?>
                    <dd><?php echo $__selected; ?></dd>
                    <select onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </dl>
                <?php echo $text_sort; ?>
            </div>
            <div class="display">
                <div class="combo">
                    <a title="<?php echo $text_list; ?>" class="view-list active" onclick="display('list');"><span class="icon icon-list">&nbsp;</span></a><a title="<?php echo $text_grid; ?>" class="view-grid" onclick="display('grid');"><span class="icon icon-grid">&nbsp;</span></a>
                </div>
                <?php echo $text_display; ?>
            </div>
            <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
            <div class="clearfix"></div>
        </div>
        <div class="product-list">
        	<?php foreach ($products as $product) { ?><div class="struct">
                	<div class="struct-right"></div>
                    <div class="struct-left"></div>
                    <div class="struct-center">
                    	<div class="line">
                            <a href="<?php echo $product['href']; ?>" class="image-link">
                                <div class="image">
                                    <?php if ($product['thumb']) { ?>
                                        <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } else { ?>
                                        <div class="no-image"></div>
                                    <?php } ?>
                                </div>
                            </a>
                            
                            <div class="right">
                                <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?> 
                                    </div>
                                <?php } ?>
                                
                                <div class="cart combo">
                                    <!--a onclick="addToCompare('<?php echo $product['product_id']; ?>');" title="<?php echo $button_compare; ?>"><span class="icon icon-compare">&nbsp;</span></a--><a onclick="addToCart('<?php echo $product['product_id']; ?>');" title="<?php echo $button_cart; ?>"><span class="icon icon-basket">&nbsp;</span></a><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" title="<?php echo $button_wishlist; ?>"><span class="icon icon-wish">&nbsp;</span></a>
                                </div>
                            </div>
                            
                            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['rating']) { ?>
                                <div class="rating" title="<?php echo $product['reviews']; ?>"><div class="stars-<?php echo $product['rating']; ?>"></div><span></span></div>
                            <?php } ?>
                            <div class="description"><?php echo $product['description']; ?></div>
                            <div class="clearfix"></div>
                        </div>
        			</div>
            </div><?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="column">
        	<div class="pagination"><?php echo $pagination; ?></div>
        </div>
    <?php } ?>
    <?php if (!$categories && !$products) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
        <div class="buttons">
        	<div class="right"><a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a></div>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
    
	<?php echo $content_bottom; ?>
</div>
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			var image = $(element).find('.image-link').html();
			var price = $(element).find('.price').html();
			var rating = $(element).find('.rating').html();
			var cart = '<div class="cart combo">' + $(element).find('.cart').html() + '</div>';
			
			image = (image != null) ? '<a href="' + $(element).find('.image-link').attr('href') + '" class="image-link">' + image + '</a>' : '';
			price = (price != null) ? '<div class="price">' + price  + '</div>' : '';
			rating = (rating != null) ? '<div class="rating" title="' + $(element).find('.rating').attr('title') + '">' + rating + '</div>' : '';
			
			

			html = 
				'<div class="struct-right"></div>' +
				'<div class="struct-left"></div>' +
				'<div class="struct-center">' +
					'<div class="line">' +
						image +
						'<div class="right">' + price + cart + '</div>' +
						'<div class="name">' + $(element).find('.name').html() + '</div>' +
						rating + 
						'<div class="description">' + $(element).find('.description').html() + '</div>' +
						'<div class="clearfix"></div>' + 
					'</div>' +
				'</div>';

						
			$(element).html(html);
		});		
		
		$('.display a').removeClass('active');			
		$('.display a.view-list').addClass('active');
		
		$.cookie('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			var image = $(element).find('.image-link').html();
			var price = $(element).find('.price').html();
			var rating = $(element).find('.rating').html();
			var cart = '<div class="cart combo">' + $(element).find('.cart').html() + '</div>';
			
			image = (image != null) ? '<a href="' + $(element).find('.image-link').attr('href') + '" class="image-link">' + image + '</a>' : '';
			price = (price != null) ? '<div class="price">' + price  + '</div>' : '';
			rating = (rating != null) ? '<div class="rating" title="' + $(element).find('.rating').attr('title') + '">' + rating + '</div>' : '';
			
			html = 
				'<div class="struct-right"></div>' +
				'<div class="struct-left"></div>' +
				'<div class="struct-center">' +
					image +
					'<div class="name">' + $(element).find('.name').html() + '</div>' +
					price + 
					cart + 
					rating + 
					'<div class="description">' + $(element).find('.description').html() + '</div>' +
				'</div>';
			
			$(element).html(html);
		});	
		
		$('.display a').removeClass('active');			
		$('.display a.view-grid').addClass('active');
		
		$.cookie('display', 'grid');
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>