<div id="notification"></div>
<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="product-grid">
      <?php foreach ($products as $product) { ?>
      <div style="margin-left:6px; margin-right:0">
        <?php if ($product['thumb']) { ?>
	
	<div href="#" class="showhim">
        <div class="image">
	<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
<div class="showme">
	<div class="description_featured" style="min-height:110px;opacity: 0.3;">	

	<!--?php echo "<p>".mb_substr(strip_tags(html_entity_decode($product['description'])), 0, 400)."...</p>"; ?--></div>
</div></a>

	<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
	
	<?php if ($product['price']) { ?>	 
       <div class="right">
                                <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                
                                <div class="cart combo">
                                    <!--a onclick="addToCompare('<?php echo $product['product_id']; ?>');" title="в сравнение"><span class="icon icon-compare">&nbsp;</span></a--><a onclick="addToCart('<?php echo $product['product_id']; ?>');" title="<?php echo $button_cart; ?>"><span class="icon icon-basket">&nbsp;</span></a><!--a onclick="addToWishList('<?php echo $product['product_id']; ?>');" title="<?php echo $button_wishlist; ?>"--><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" title="в закладки"><span class="icon icon-wish">&nbsp;</span></a>
                                </div>
                            </div>
        <div style="margin-top:24px;"></div>

	</div>	
<div class="showme">
	<?php if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <?php } ?>
	
</div></div>

	
        <?php } ?>
    

 
        
      </div>
      <?php } ?>
    </div>
  </div>
</div>
