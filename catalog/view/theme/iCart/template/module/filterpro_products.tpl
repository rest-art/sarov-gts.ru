<script>
    $(".clear_filter").click(function () {
        $("#filterpro img").removeClass("selected");
        $("#filterpro select").val("");
        $("#filterpro :input").each(function () {
            if ($(this).is(":checked")) {
                $(this).attr("checked", false)
            }
        });
        var b = $("#slider-range").slider("option", "min");
        var a = $("#slider-range").slider("option", "max");
        $("#slider-range").slider("option", {values:[b, a]});
        $("#min_price").val(b);
        $("#max_price").val(a);
	$("div[id^=slider-range-]").each(function(index, element) {
	    var id = this.id.replace(/[^\d]/g, '');

	    var b = $(element).slider("option", "min");
	    var a = $(element).slider("option", "max");
	    
	    hs = $(element).slider();
	    hs.slider("option", {values:[b, a]});
	    hs.slider("option","slide").call(hs, null, { handle: $(".ui-slider-handle", hs), values:[b, a] });

	    $("#attribute_value_"+id+"_min").val('');
	    $("#attribute_value_"+id+"_max").val('');

	});    });</script>

<?php foreach ($products as $product) { ?><div class="struct">
                	<div class="struct-right"></div>
                    <div class="struct-left"></div>
                    <div class="struct-center">
                    	<div class="line">
                            <a href="<?php echo $product['href']; ?>" class="image-link">
                                <div class="image">
                                    <?php if ($product['thumb']) { ?>
                                        <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
                                    <?php } else { ?>
                                        <div class="no-image"></div>
                                    <?php } ?>
                                </div>
                            </a>
                            
                            <div class="right">
                                <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?> 
                                        <?php /* echo " (за <!--$text_sellunit-->"; ?> 
<?php if ($product['sellunit'] == 1) echo "шт."; ?>
<?php if ($product['sellunit'] == 2) echo "м<sup style='font-size: 8px;
vertical-align: top;'>2</sup><span class='mpog'> пог.</span>"; ?>
<?php if ($product['sellunit'] == 3) echo "упак."; ?>
<?php if ($product['sellunit'] == 4) echo "кг."; ?>
<?php if ($product['sellunit'] == 5) echo "пар."; ?>
<?php if ($product['sellunit'] == 6) echo "м пог."; ?>
<?php echo ")" */ ?>
                                    </div>
                                <?php } ?>
                                
                                <div class="cart combo">
                                    <!--a onclick="addToCompare('<?php echo $product['product_id']; ?>');" title="<?php echo $button_compare; ?>"><span class="icon icon-compare">&nbsp;</span></a--><a onclick="addToCart('<?php echo $product['product_id']; ?>');" title="<?php echo $button_cart; ?>"><span class="icon icon-basket">&nbsp;</span></a><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" title="<?php echo $button_wishlist; ?>"><span class="icon icon-wish">&nbsp;</span></a>
                                </div>
                            </div>
                            
                            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                            <?php if ($product['rating']) { ?>
                                <div class="rating" title="<?php echo $product['reviews']; ?>"><div class="stars-<?php echo $product['rating']; ?>"></div><span></span></div>
                            <?php } ?>
                            <div class="description"><?php echo $product['description']; ?></div>
                            <div class="clearfix"></div>
                        </div>
        			</div>
            </div><?php } ?>