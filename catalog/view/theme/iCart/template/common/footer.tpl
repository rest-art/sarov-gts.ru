</div>
</div>
<div class="footer">
<div id="footer-top">
	<div class="footer-container">
	<ul class="social-icons">
	<li class="email"><a href="mailto:sarov-market@mail.ru">E-Mail</a></li><!--li class="twitter"><a  href="https://twitter.com/" target="_blank">Twitter</a></li>
	<li class="facebook"><a href="https://www.facebook.com/" target="_blank">Facebook</a></li>
	<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank">LinkedIn</a></li>
	
	<!--li class="tumblr"><a href="http://www.tumblr.com/" target="_blank">Tumblr</a></li-->
	<!--li class="dribbble"><a href="http://dribbble.com" target="_blank">Dribbble</a></li-->
	<!--li class="youtube"><a href="http://www.youtube.com/" target="_blank">Youtube</a></li-->
	<!--li class="vimeo"><a href="http://vimeo.com/" target="_blank">Vimeo</a></li>
	<li class="skype"><a href="http://skype.com/" target="_blank">Skype</a></li-->
		</ul>
		<a href="#" class="scrollToTop">Наверх</a>
		<div class="clearfix"></div>
	</div>
</div>
<div class="footer-container">
	<div class="grid4column" style="width:240px;">
		<h3>О компании</h3>
		Компания "ГорТехСнаб" занимается продажей строительных материалов. Наш ассортимент насчитывает более 5000 наименований товаров для ремонта и строительства. 
	</div>
	
	<div class="grid4column">
		<h3><?php echo $text_service; ?></h3>
		<ul>
		<?php foreach ($informations as $information) { ?>
      		<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      		<?php } ?>
      		<li><a href="<?php echo $contact; ?>"><!--<?php echo $text_contact; ?>-->Карта проезда</a></li>
      		<!--li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li-->
      		<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
		</ul>
	</div>
	
	<div class="grid4column" style="width:300px;">
<? /* */ ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.cycle.js"></script><br><br>
<style>
.banner {
overflow:hidden !Important;
max-height:90px !important
}
</style>
 <div id="banner0" class="banner">
      <div><a href="http://gilmast.com" target="_blank"><img src="http://sarov-gts.ru/image/cache/data/bnr/3-240x90.jpg" alt="Дизайн интерьеров" title="Дизайн интерьеров" /></a></div>
        <div><a href="http://gilmast.com" target="_blank"><img src="http://sarov-gts.ru/image/cache/data/bnr/2-240x90.jpg" alt="Отделка и ремонт квартир" title="Отделка и ремонт квартир" /></a></div>
        <div><a href="http://gilmast.com" target="_blank"><img src="http://sarov-gts.ru/image/cache/data/bnr/1-240x90.jpg" alt="Гильдия Мастеров" title="Гильдия Мастеров" /></a></div>
        <div><a href="http://gilmast.com" target="_blank"><img src="http://sarov-gts.ru/image/cache/data/bnr/4-240x90.jpg" alt="8 904 780 50 85" title="8 904 780 50 85" /></a></div>
        <div><a href="http://gilmast.com" target="_blank"><img src="http://sarov-gts.ru/image/cache/data/bnr/0-240x90.jpg" alt="ГорТехСнаб рекомендует" title="ГорТехСнаб рекомендует" /></a></div>
    </div>
<script type="text/javascript"><!--
var banner = function() {
	$('#banner0').cycle({
		before: function(current, next) {
			$(next).parent().height($(next).outerHeight());
		}, timeout:  100
	});
}

setTimeout(banner, 2000);
//--></script></div>

		<!--h3><?php echo $text_account; ?></h3>
		<ul>
      		<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      		<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      		<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
      		<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
		</ul-->
	</div>
	<!--div class="grid4column" style="width:150px;">
		<h3><?php echo $text_extra; ?></h3>
		<ul>
      		<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
      		<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
      		<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
      		<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
		</ul>
	</div-->	
		
	<div class="grid4column lastcolumn">
		<h3>Контакты</h3>
		<p></p>
		<ul class="contact-us-widget">
			<li class="address"><a href="http://maps.yandex.ru/?um=gzZCSG6cCzvobH56CbTyLLGvMQ6pJmYa&l=sat%2Cskl%2Csat" target="_blank">г. Саров, дор. Мал. Коммунальная,<br /> д.8</a></li>
			<li class="phone">8 (83130) 33-170</li>
			<li class="email"><a href="mailto:sarov-market@mail.ru">sarov-market@mail.ru</a></li>
		</ul>				
	</div>		
	<div class="clearfix"></div>
	</div>
		
	<div id="footer-base">
	<div id="footer-base-shadow">
		<div class="footer-container">				
		<div class="logo-footer"><a href="index.php"><img src="catalog/view/theme/iCart/image/logo-footer.png" alt="Footer Logo"></a></div>	
			<ul class="footer-nav" style="margin-top:19px">


<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=23332579&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/23332579/2_0_FF5D22FF_E83D02FF_1_visits"
style="width:80px; height:31px; border:0; position:absolute; 
margin-left: 15px;
margin-top: -4px;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (визиты)" onclick="try{Ya.Metrika.informer({i:this,id:23332579,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter23332579 = new Ya.Metrika({id:23332579,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/23332579" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img style='position: absolute;display:none;margin-left: 15px;margin-top: 12px;' src='//counter.yadro.ru/hit?t25.7;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
<li style="margin-top: 30px;
border: 0;">Разработка сайта <a href="http://rest-art.info" target="_blank" title="Разработка сайтов">РеСтарт</a></li>
			</ul>		
		<div class="clearfix"></div>
		</div>
		</div>
 

</div>
</div>
</div>
</body>
</html>