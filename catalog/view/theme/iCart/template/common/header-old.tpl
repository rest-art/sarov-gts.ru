<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script>$(document).ready(function(){
	$('.scrollToTop').click(function(){
    $('html, body').animate({ scrollTop: 0 }, 600);
    return false;
    });				
});
</script>

<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>

<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/iCart/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php echo $google_analytics; ?>
</head>
<body
<?php 		
	$page = '';
	if(isset($this->request->get['route'])){
		$page = $this->request->get['route']; 
	}
	if($page == "common/home" || $page == ''){
		echo 'class="home"';

	}elseif($page == "product/category"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
		echo 'class="' . strtolower($page) . " category" . '"';		
	}

	elseif($page == "account/wishlist" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "account/account" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "checkout/cart" || $page == ''){
		echo 'class="home"';	
	}
	elseif($page == "checkout/checkout" || $page == ''){
		echo 'class="home"';	
	}

	elseif($page == "product/product"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
		echo 'class="' . strtolower($page) . " product_page" . '"';		
	}
	elseif($page !== "common/home"){
		$titleName = explode(' ',$title);
		$page = $titleName[0];	
			if(isset($titleName[1])){
				$page = $titleName[0] . "_" . $titleName[1];
			}
		echo 'class="' . strtolower($page) . '"';				
	}


	

?>>

<style type="text/css">

#container_bg {
	width:1000px;
	margin-left: auto;
	margin-right: auto;
	text-align: left;
   	background: none;
	border: none;
	margin-bottom:none;
	padding-left:none;
	padding-right:none;
	padding-top:0;
	border-radius:none;
	padding-top:0;
	margin-top:0;
}

</style>

<div class="linkz">
<ul>
<li <?php if($page == "common/home" || $page == ''){echo 'class="active"';} ?>><a class="home" href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
<li <?php if($page == "account/wishlist" || $page == 'account/wishlist'){echo 'class="active"';} ?>><a class="wishlist" href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
<li <?php if($page == "account/account" || $page == 'account/account'){echo 'class="active"';} ?>><a class="account" href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
<li <?php if($page == "checkout/cart" || $page == 'checkout/cart'){echo 'class="active"';} ?>><a class="cart" href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>

<li <?php if($page == "checkout/checkout" || $page == 'checkout/checkout'){echo 'class="active"';} ?>><a class="checkout"href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
</ul>
</div>

<div id="container">
<div id="header">



  <?php if ($logo) { ?> <div id="logo"><a href="<?php echo $home; ?>"><img alt="<?php echo $name; ?>"  title="<?php echo $name; ?>" src="catalog/view/theme/iCart/image/logo.png"/></a></div>  <?php } ?>
 

<?php echo $language; ?>

<?php echo $currency; ?>

<?php echo $cart; ?>

<div>  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
  </div>

  <div id="search">
    <div class="button-search"></div>
    <?php if ($filter_name) { ?>
    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
    <?php } else { ?>
    <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#888';" />
    <?php } ?>
  </div>
</div>

</div>


<?php if ($categories) { ?>
<div id="menu">
  <ul>
  
    <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
	

      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>

        <ul>	

          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) {  if (++$posted == 9) {
              echo '</ul><ul>';
              $posted = 1;
            } ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
         </ul>
        <?php } ?>
      </div>
<!--img src="catalog/view/theme/iCart/image/arrow-down_menu.png" style="margin-bottom:-4px;margin-right:11px;margin-left:-14px;"/-->

	<?php } ?>
    </li>
    <?php } ?>


<li class="lastedmenu"><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11800" class="lastedmenu">ВСЕ КАТЕГОРИИ</a>
      	

      <div style="margin-left:-825px !important">
        
        <ul>	

<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11800">ГЕРМЕТИКИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11794">ГИПСОКАРТОН</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=13594">ДВЕРНАЯ ФУРНИТУРА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=3845">ДВЕРНЫЕ ЗАМКИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12781">ИЗОЛЯЦИОННЫЕ МАТЕРИАЛЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9480">ИНВЕНТАРЬ САДОВЫЙ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12770">ИНВЕНТАРЬ СНЕГОУБОРОЧНЫЙ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12769">ИНВЕНТАРЬ ХОЗЯЙСТВЕННЫЙ</a></li></ul> <ul>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=13710">КИРПИЧ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9476">КЛЕИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12194">КОМПРЕССОРЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=4102">КРАСКОПУЛЬТЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12780">ЛЕНТЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9483">МЕТИЗЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=10596">ОБОИ ДЛЯ СТЕН</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9474">ПЕНА МОНТАЖНАЯ</a></li></ul> <ul>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11525">ПЛИНТУС НАПОЛЬНЫЙ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12196">ПОДВЕСНЫЕ ПОТОЛКИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12788">ПРОФИЛЬ МЕТАЛЛИЧЕСКИЙ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9479">РАСХОДНЫЕ МАТЕРИАЛЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9477">РУЧНОЙ ИНСТРУМЕНТ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=8889">САДОВАЯ ТЕХНИКА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=10631">САНТЕХНИКА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11324">СВАРОЧНОЕ ОБОРУДОВАНИЕ</a></li></ul> <ul>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12603">СВЕТОТЕХНИКА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9484">СЕТКИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12768">СПЕЦОДЕЖДА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9481">СРЕДСТВА ИНДИВИДУАЛЬНОЙ ЗАЩИТЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11429">ТЕПЛОНОСИТЕЛИ ДЛЯ СИСТЕМ ОТОПЛЕНИЯ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=10606">ТЕПЛОТЕХНИКА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9487">ТОВАРЫ ПОД ЗАКАЗ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=13468">УГОЛОК ПЛАСТИКОВЫЙ</a></li></ul> <ul>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11810">УПЛОТНИТЕЛЬ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=12195">ФАНЕРА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=13001">ЭЛЕКТРИКА</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9485">ЭЛЕКТРОДЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9478">ЭЛЕКТРОИНСТРУМЕНТ</a></li>
<!--li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9469">Продукция Фаворит</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9471">ЛАКОКРАСОЧНЫЕ МАТЕРИАЛЫ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9473">РАСТВОРИТЕЛИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=9470">СУХИЕ СМЕСИ</a></li>
<li><a href="http://gortehsnab-ru.1gb.ru/upload/index.php?route=product/category&amp;path=11436">КЕРАМОГРАНИТ</a></li-->


                             </ul>
              </div>

	    </li>


  </ul>
</div>
</div>


<div id="container_bg">
<?php } ?>

