<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title;
if (isset($_GET['page'])) {
echo " | echo $page_w; ". ((int) $_GET['page']);
} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) {
echo " | echo $page_w; ". ((int) $_GET['page']);
} ?>" />
<?php } ?>
<meta name='yandex-verification' content='7fc58a96ae279ed2' />
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<link rel="icon" href="http://svetanna.ru/favicon-new.png" 
type="image/x-icon" />
<link rel="shortcut icon" href="http://svetanna.ru/favicon-new.png"
type="image/x-icon" />
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet2.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/commonproof.js"></script>
<script type="text/javascript" src="catalog/view/javascript/notify.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript">
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
</script>
<?php } ?>
<?php echo $google_analytics; ?>
<script>
$(document).ready(function() {
$.cookie('display') = 'grid';});
</script>
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter22844725 = new Ya.Metrika({id:22844725,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/22844725" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript"> 
_shcp = []; _shcp.push({widget_id : 636315, widget : "Chat"}); (function() { var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true; hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://widget.siteheart.com/apps/js/sh.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(hcc, s.nextSibling); })();
</script>
<div id="top">
<div class="header" style="height: 30px; background: none;">
		<div id="welcome">
			<?php if (!$logged) { ?>
			<?php echo $text_welcome; ?>
			<?php } else { ?>
			<?php echo $text_logged; ?>
			<?php } ?>
		</div>
		<div class="tell">Тел.: +7 (499) 394-28-33</div>
		
				<?php echo $cart; ?>
		
<div id="search">
<div class="button-search"></div>
<input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />

		</div>
</div></div>

<div id="top3"></div>

<div id="top2">
<div class="header">
		<?php if ($logo) { ?>
			<?php if (strlen ($_SERVER['REQUEST_URI']) > 1 ){ ?>
				<div id="logo"><a href="/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
			<?php } else  { ?>
				<div id="logo"><a href="/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
			<?php } ?>
		<?php } ?>
		

</div></div>


	<div id="menu">
	<div class="menu5">
  <ul>
    <li><a href="/">Главная</a></li>
    <li><a href="/factory">Фабрики</a></li>
    <li><a href="/delivery">Доставка</a></li>
    <li><a href="/payment">Оплата</a></li>
    <li><a href="/gallery">Портфолио</a></li>
    <li><a href="/contacts">Контакты</a></li>
  </ul>
</div></div>	
</div></div>


<div style="margin: 0px auto; position: relative; width: 930px;">
<div id="container">




<div id="notification"></div>
